# REGLIS-CUTTINGS #

reglis mock services

## Setup ##

### Requirements ###
* Linux (*with shared memory of at least 256mb*: sysctl -w vm.max_map_count=262144)
* Docker
* git
* wget

### Installation ###
1. download the installation script here https://goo.gl/xTVffR
2. execute the script 
```
#!shell
sh buildregliscuttings.sh
```

once the docker image creation succeeded, you can run it using the following syntax:

```
#!shell
docker run -ti -v /usr/share/elasticsearch/data -p 8180:8080 -p 9200:9200 -e "http.host=0.0.0.0" -e "transport.host=127.0.0.1"  reglis-cuttings
```



## Usage ##

* the search feature is implemented with Elasticsearch 5.x - dsl search query language is available*
* once the entity is known by its ID (personId,OrganisationId,emsId,...) use the rest API's to get the record or modify it.

## REST ##

### GET person by id ###
```
#!shell
curl -u licorice:-GLyCyrrhi3a- http://localhost:8180/talendmdm/data/person/1?container=MASTER
```

### GET organisation by id ###
```
#!shell
curl -u licorice:-GLyCyrrhi3a- http://localhost:8180/talendmdm/data/organisation/1?container=MASTER
```

### POST person (create) ###
```
#!shell
curl -v -XPOST -u licorice:-GLyCyrrhi3a- http://localhost:8180/talendmdm/data/REGLIS -d @personCreate.xml
```

### PUT person (update) ###
```
#!shell
curl -v -XPUT -u licorice:-GLyCyrrhi3a- http://localhost:8180/talendmdm/data/REGLIS -d @personUpdate.xml
```

### GET EMS by id ###
```
#!shell
curl -u licorice:-GLyCyrrhi3a- http://localhost:8180/talendmdm/data/SSP_EMS/1?container=MASTER
```
# Search for persons
```
#!shell
curl -u licorice:-GLyCyrrhi3a- http://localhost:9200/personnes/person/_search?q=*
```

# Search for organisations
```
#!shell
curl -u licorice:-GLyCyrrhi3a- http://localhost:9200/organisations/organisation/_search?q=*
```

# Search for EMS (social health-care institutions)
```
#!shell
curl -u licorice:-GLyCyrrhi3a- http://localhost:9200/ssp_ems_collection/ssp_ems/_search?q=*
```


### Data samples ###
#personCreate.xml#
```
#!xml
<person>
    <personId></personId>
    <idReglis></idReglis>
    <identifiers>
        <identifier>
            <type>CH.AVS</type>
            <ident>7560000000000</ident>
        </identifier>
    </identifiers>
    <status>0</status>
    <firstName>Michael</firstName>
    <lastName>Jackson</lastName>
    <active>true</active>
</person>
```

#personUpdate.xml#
```
#!xml
<person>
    <personId>603314</personId>
    <identifiers>
        <identifier>
            <type>CT.VD</type>
            <ident>111111111111</ident>
        </identifier>
    </identifiers>
    <status>100</status>
    <firstName>prenoTest2</firstName>
    <lastName>TestWSRest</lastName>
    <active>false</active>
</person>
```

#REGLIS model#
**reglis-model.xsd