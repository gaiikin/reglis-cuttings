#!/bin/bash
echo ".__  _ |o ____  _|__|_o._  _  _   gaya.rime#vd.ch"
echo "|(/_(_|||_> (_|_||_ |_|| |(_|_>              v1.0"
echo "     _|                    _|"

ansigreen='\e[0;32m' 
ansired='\e[0;31m'
ansiendColor='\e[0m'
function OK {
	printf "$ansigreen OK $ansiendColor\n"
}
function KO {
	printf "$ansired   KO $ansiendColor\n"
}

function buildDockerImage {
	printf "building docker image      : "
	docker build --tag=reglis-cuttings . 	>/dev/null 2>&1
	if [ $? -eq 0 ]
        then
                OK
		printf "\nImage built successfully\n\n"
		echo "start the container with the following commandline:"
		echo "docker run -ti -v /usr/share/elasticsearch/data -p 8180:8080 -p 9200:9200 -e \"http.host=0.0.0.0\" -e \"transport.host=127.0.0.1\"  reglis-cuttings"
        else
                KO
		echo "An error occured while building the docker image..."
        fi

}

function setupReglisCuttingsContainer {
	printf "setup git project          : "
	curdir=`pwd`
	tmpdir=`mktemp -d -t reglis.XXXXXXXX`
	cd $tmpdir
	git clone -n https://gaiikin@bitbucket.org/gaiikin/reglis-cuttings.git --depth 1 >/dev/null 2>&1
	if [ $? -eq 0 ]
	then
		cd reglis-cuttings 
		git checkout HEAD Dockerfile 
		OK
		buildDockerImage
	else
		KO
	fi
	cd $curdir
	rm -rf $tmpdir

}


#check prerequisites
printf "checking prerequisites     : "
#check whether wget is available
if [ ! -x /usr/bin/wget ] ; then
    # some extra check if wget is not installed at the usual place
    command -v wget >/dev/null 2>&1 || { KO; echo >&2 "Please install wget or set it in your path. Aborting."; exit 1; }
fi
#check whether git is available
if [ ! -x /usr/bin/git ] ; then
    # some extra check if git is not installed at the usual place
    command -v wget >/dev/null 2>&1 || { KO; echo >&2 "Please install git or set it in your path. Aborting."; exit 1; }
fi
#check whether docker is running
which docker >/dev/null 2>&1
if [ $? -eq 0 ]
then
    docker ps >/dev/null 2>&1 
    if [ $? -eq 0 ]
    then
	OK
	setupReglisCuttingsContainer
    else
	KO
	echo "Please start up docker before running this script."; exit 1;
    fi
else
    KO
    echo "Please install docker or set it in your path. Aborting."; exit 1; 
fi

