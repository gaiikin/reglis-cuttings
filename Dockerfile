FROM docker.elastic.co/elasticsearch/elasticsearch:5.2.1

USER root
ENV JAVA_HOME /usr
ENV CATALINA_HOME /usr/share/tomcat
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin:$CATALINA_HOME/scripts
RUN wget https://bitbucket.org/gaiikin/reglis-cuttings/raw/b1ae5850535afeb93c853fe65873758fe33c2aac/elasticsearch.yml -O /usr/share/elasticsearch/config/elasticsearch.yml
RUN wget https://bitbucket.org/gaiikin/reglis-cuttings/raw/8547032eae130539b015024acccd3439fc6b918e/mockmdmservice -O /etc/init.d/mockmdmservice
RUN chmod 755 /etc/init.d/mockmdmservice
RUN chown elasticsearch:elasticsearch config/elasticsearch.yml
ENV TOMCAT_MAJOR 8
ENV TOMCAT_VERSION 8.5.20
RUN wget http://mirror.switch.ch/mirror/apache/dist/tomcat/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
 tar -xvf apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
 rm apache-tomcat*.tar.gz && \
 mv apache-tomcat* ${CATALINA_HOME} && \
 rm -rf ${CATALINA_HOME}/webapps/* && \
 chown -R elasticsearch:elasticsearch ${CATALINA_HOME} && \
 chmod +x ${CATALINA_HOME}/bin/*sh
RUN wget https://bitbucket.org/gaiikin/reglis-cuttings/raw/b1ae5850535afeb93c853fe65873758fe33c2aac/reglis_mock.war -O ${CATALINA_HOME}/webapps/talendmdm.war
RUN wget https://bitbucket.org/gaiikin/reglis-cuttings/raw/b1ae5850535afeb93c853fe65873758fe33c2aac/AlimEKS_Mock_0.1.zip -O /etc/init.d/initload.zip
CMD ["/etc/init.d/mockmdmservice","start"]
USER elasticsearch
EXPOSE 8080
